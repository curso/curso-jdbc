CREATE TABLE Persona
(
  nombre character varying(50),
  apellido character varying(50),
  identificacion character varying(15),
  genero character varying(1),
  estadoCivil character varying(1)
);
ALTER TABLE Persona
  OWNER TO root;

INSERT INTO Persona (nombre, apellido, identificacion, genero, estadoCivil) values ('Juan','Perez','1102809875', 'M','S');
INSERT INTO Persona (nombre, apellido, identificacion, genero, estadoCivil) values ('Juana','Perez','1102809877', 'F','S');
INSERT INTO Persona (nombre, apellido, identificacion, genero, estadoCivil) values ('Hernan','Gales','1102809875', 'M','C');
INSERT INTO Persona (nombre, apellido, identificacion, genero, estadoCivil) values ('Paula','Pardo','1102809875', 'F','C');