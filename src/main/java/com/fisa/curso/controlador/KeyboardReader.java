package com.fisa.curso.controlador;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class KeyboardReader {
	private BufferedReader keyboard;
	
	public KeyboardReader() {
		keyboard = new BufferedReader(new InputStreamReader(System.in));
	}
	
	public String readline(){
		String valor = null;
		try {
			valor = keyboard.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return valor;
	}	
}
