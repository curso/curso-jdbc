package com.fisa.curso.controlador;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import com.fisa.curso.modelo.EstadoCivil;
import com.fisa.curso.modelo.Genero;
import com.fisa.curso.modelo.Persona;

public class PersonaController {

	public void create(Connection connection, Persona persona) throws Exception {
		String INSERT_SQL = "INSERT INTO Persona (nombre, apellido, identificacion, genero, estadoCivil) values (?,?,?,?,?);";

		PreparedStatement statement = connection.prepareStatement(INSERT_SQL);
		statement.setString(1, persona.getNombre());
		statement.setString(2, persona.getApellido());
		statement.setString(3, persona.getIdentificacion());
		statement.setString(4, persona.getGenero().name());
		statement.setString(5, persona.getEstadoCivil().name());

		statement.executeUpdate();
		statement.close();
	}

	public List<Persona> retrieve(Connection connection) throws Exception {
		List<Persona> personas = new LinkedList<Persona>();
		Statement statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery("SELECT * FROM Persona ORDER BY identificacion");
		while (resultSet.next()) {

			String nombre = resultSet.getString("nombre");
			String apellido = resultSet.getString("apellido");
			String identificacion = resultSet.getString("identificacion");
			String generoValue = resultSet.getString("genero");
			String estadoCivilValue = resultSet.getString("estadoCivil");

			Genero genero = Genero.valueOf(generoValue);
			EstadoCivil estadoCivil = EstadoCivil.valueOf(estadoCivilValue);

			Persona persona = new Persona();
			persona.setNombre(nombre);
			persona.setApellido(apellido);
			persona.setIdentificacion(identificacion);
			persona.setEstadoCivil(estadoCivil);
			persona.setGenero(genero);

			System.out.println(persona);
			personas.add(persona);
		}
		resultSet.close();
		statement.close();
		return personas;
	}

}
