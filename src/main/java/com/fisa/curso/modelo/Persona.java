package com.fisa.curso.modelo;

import java.util.List;

public class Persona {
	String nombre;
	String apellido;
	String identificacion;
	Genero genero;
	EstadoCivil estadoCivil;
	
	List<Contacto> contactos;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	public Genero getGenero() {
		return genero;
	}

	public void setGenero(Genero genero) {
		this.genero = genero;
	}

	public EstadoCivil getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(EstadoCivil estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public List<Contacto> getContactos() {
		return contactos;
	}

	public void setContactos(List<Contacto> contactos) {
		this.contactos = contactos;
	}	
	
	@Override
	public String toString() {
		String valor = identificacion+"\t"+nombre + "\t"+ apellido+"\t"+estadoCivil+"\t"+genero;
		return valor;
	}
}
