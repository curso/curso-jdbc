package com.fisa.curso;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.fisa.curso.controlador.KeyboardReader;
import com.fisa.curso.controlador.PersonaController;
import com.fisa.curso.modelo.EstadoCivil;
import com.fisa.curso.modelo.Genero;
import com.fisa.curso.modelo.Persona;

public class Ejecutor {
	public static void main(String[] args) {
		try {
			Class.forName("org.postgresql.Driver");
			Connection connection = DriverManager.getConnection(
					"jdbc:postgresql://localhost:5432/curso", "root", "admin");

			Persona persona = leerPersona();
			PersonaController controlador = new PersonaController();
			controlador.create(connection, persona);
			controlador.retrieve(connection);
			//controlador.update(connection);
			//controlador.delete(connection);

			connection.close();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	private static Persona leerPersona() {
		KeyboardReader teclado = new KeyboardReader();
		
		System.out.println("NUEVA PERSONA");
		System.out.println("=============");
		System.out.print("Identificacion: ");
		String identificacion= teclado.readline();
		System.out.print("Nombre:         ");
		String nombre = teclado.readline();
		System.out.print("Apellido:       ");
		String apellido = teclado.readline();
		System.out.print("Genero:         ");
		String genero = teclado.readline();
		System.out.print("Estado civil:   ");
		String estadoCivil = teclado.readline();
		
		Persona persona = new Persona();
		persona.setIdentificacion(identificacion);
		persona.setNombre(nombre);
		persona.setApellido(apellido);
		persona.setEstadoCivil(EstadoCivil.valueOf(estadoCivil));
		persona.setGenero(Genero.valueOf(genero));
		System.out.print("Guardando .... "+persona);
		
		return persona;
	}


}
