package com.fisa.curso.controlador;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.fisa.curso.modelo.EstadoCivil;
import com.fisa.curso.modelo.Genero;
import com.fisa.curso.modelo.Persona;


public class PersonaControllerTest {
	
	PersonaController personaController;
	
	static Connection connection;
	
	@BeforeClass
	public static void inicializarBaseDatos(){
		try {
			Class.forName("org.postgresql.Driver");
			connection = DriverManager.getConnection(
					"jdbc:postgresql://localhost:5432/curso", "root", "admin");	
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Before
	public void inicializar(){
		personaController = new PersonaController();
		Statement statement;
		try {
			statement = connection.createStatement();
			statement.execute("DELETE FROM Persona");
			statement.execute("INSERT INTO Persona (nombre, apellido, identificacion, genero, estadoCivil) values ('Juan','Perez','1111111111', 'M','S')");
			statement.execute("INSERT INTO Persona (nombre, apellido, identificacion, genero, estadoCivil) values ('Juana','Perez','2222222222', 'F','S')");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void createTest() throws Exception{
		List<Persona> personas = personaController.retrieve(connection);
		Assert.assertEquals(2, personas.size());
		Persona persona = createPersona();
		personaController.create(connection, persona);
		personas = personaController.retrieve(connection);
		Assert.assertEquals(3, personas.size());
	}
	
	@Test
	public void retrieveTest() throws Exception{
		List<Persona> personas = personaController.retrieve(connection);
		Assert.assertEquals(2, personas.size());
		Persona persona = personas.get(0);
		Assert.assertEquals("1111111111", persona.getIdentificacion());
	}
	
	private Persona createPersona(){
		Persona persona = new Persona();
		persona.setIdentificacion("3333333333");
		persona.setNombre("Luis");
		persona.setApellido("Ortiz");
		persona.setEstadoCivil(EstadoCivil.S);
		persona.setGenero(Genero.M);
		return persona;
	}
}
